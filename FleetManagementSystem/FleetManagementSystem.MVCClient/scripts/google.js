﻿var markers = [];

function initMap() {
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat:39.8282, lng: -98.5795},
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    directionsDisplay.setMap(map);

    var onClickHandler = function () {
        calculateAndDisplayRoute(directionsService, directionsDisplay);
    }

    window.addEventListener('load', onClickHandler, false);
 //   document.getElementById('submit').addEventListener('onclick', onClickHandler);
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    directionsService.route({
        origin: document.getElementById('address').value,
        destination: document.getElementById('address2').value,
        travelMode: google.maps.TravelMode.DRIVING
    }, function (response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}

