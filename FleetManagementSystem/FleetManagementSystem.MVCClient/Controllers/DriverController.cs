﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FleetManagementSystem.MVCClient.ViewModels;
using domain = FleetManagementSystem.Domain;
using FleetManagementSystem.MVCClient.Models;

namespace FleetManagementSystem.MVCClient.Controllers
{
    public class DriverController : Controller
    {
        private domain.DomainHelper dh = new domain.DomainHelper();
        // GET: Driver
        public ActionResult Index()
        {
            return View("Index","_Layout");
        }

        [HttpGet]
        public ActionResult Assignment(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var driverList = dh.GetDriverList();
                    var drivers = ClientHelper.GetDriverList(driverList);
                    var driver = drivers.FirstOrDefault(d => d.Id == id);

                    var aList = dh.GetAssignList();
                    var assignments = ClientHelper.GetAssignmentList(aList);

                    var assignment = assignments.FirstOrDefault(a => a.driverId == driver.Id);
                    var daList = new List<ViewModels.DriverAssignment>();
                    foreach (var item in assignments)
                    {
                        if (item.driverId == driver.Id)
                        {
                            daList.Add(item);
                        }
                    }
                    return View("Assignment", "_Layout", daList);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                return View("Index", "_Layout");
            }
        }

        [HttpPost]
        public ActionResult CompleteSubmit(DriverAssignment c)
        {
            var co = 0;
            if( c.completeB )
            {
                co = 1;
            }
            var b = new domain.AssignmentDTO()
            {
                aId = c.aId,
                complete = co
            };

            dh.updateC(b);
            try
            {
                    var driverList = dh.GetDriverList();
                    var drivers = ClientHelper.GetDriverList(driverList);
                    var driver = drivers.FirstOrDefault(d => d.Id == c.driverId);

                    var aList = dh.GetAssignList();
                    var assignments = ClientHelper.GetAssignmentList(aList);

                    var assignment = assignments.FirstOrDefault(a => a.driverId == driver.Id);
                    var daList = new List<ViewModels.DriverAssignment>();
                    foreach (var item in assignments)
                    {
                        if (item.driverId == driver.Id)
                        {
                            daList.Add(item);
                        }
                    }
                    return View("Assignment", "_Layout", daList);
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet]
        public ActionResult MapRouter(int id)
        {
            var aList = dh.GetAssignList();
            var assignments = ClientHelper.GetAssignmentList(aList);

            var assignment = assignments.FirstOrDefault(a => a.aId == id);
            return View("MapRouter", "_Layout", assignment);
        }
    }
}