﻿using FleetManagementSystem.MVCClient.Models;
using FleetManagementSystem.MVCClient.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using domain = FleetManagementSystem.Domain;

namespace FleetManagementSystem.MVCClient.Controllers
{
    public class DispatchController : Controller
    {
        private domain.DomainHelper dh = new domain.DomainHelper();
        // GET: Dispatch
        public ActionResult Index()
        {
            return View("Index","_Layout");
        }

        public ActionResult DriversList()
        {
            var driverList = dh.GetDriverList();
            var drivers = ClientHelper.GetDriverList(driverList);
            return View("DriversList","_Layout",drivers);
        }

        public ActionResult DriverAssignment(int id)
        {
            var driverList = dh.GetDriverList();
            var drivers = ClientHelper.GetDriverList(driverList);
            var driver = drivers.FirstOrDefault(d => d.Id == id);

            var aList = dh.GetAssignList();
            var assignments = ClientHelper.GetAssignmentList(aList);

            var assignment = assignments.FirstOrDefault(a => a.driverId == driver.Id);
            var daList = new List<ViewModels.DriverAssignment>();
            foreach(var item in assignments)
            {
                if(item.driverId == driver.Id)
                {
                    daList.Add(item);
                }
            }
            
            return View("DriverAssignment", "_Layout",daList);
        }

        public ActionResult MapRouter(int id)
        {
            var aList = dh.GetAssignList();
            var assignments = ClientHelper.GetAssignmentList(aList);

            var assignment = assignments.FirstOrDefault(a => a.aId == id);
            return View("MapRouter", "_Layout", assignment);
        }

        public ActionResult AddAssignment(int id)
        {
            var driverList = dh.GetDriverList();
            var drivers = ClientHelper.GetDriverList(driverList);
            var driver = drivers.FirstOrDefault(d => d.Id == id);

            var aList = dh.GetAssignList();
            var assignments = ClientHelper.GetAssignmentList(aList);
            var assignment = assignments.FirstOrDefault(a => a.driverId == driver.Id);

            return View("AddAssignment", "_Layout",assignment);
        }

        [HttpPost]
        public ActionResult AddAssignment(DriverAssignment adm)
        {
            var s = adm.sAddressProp;
            var e = adm.eAddressProp;
            s = s.Replace(" ", "-");
            e = e.Replace(" ", "-");
            try
            {
                if(ModelState.IsValid)
                {
                    string c = adm.completeProp;
                    int co = 0;

                    if (c == "Yes" || c == "yes")
                    {
                        co = 1;
                    }

                    var a = new domain.AssignmentDTO()
                    {
                        driverId = adm.driverId,
                        sAddress = s,
                        eAddress = e,
                        details = adm.detailsProp,
                        complete = co
                    };
                    if(dh.AddAssignment(a))
                    {
                        Console.WriteLine(adm.driverId);
                        return View("Index", "_Layout");
                    }
                }
                throw new Exception();
            }
            catch
            {
                return View("Index", "_Layout");
            }
        }
    }
}