﻿using FleetManagementSystem.MVCClient.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using domain = FleetManagementSystem.Domain;

namespace FleetManagementSystem.MVCClient.Models
{
    public class ClientHelper
    {
        public static List<Drivr> GetDriverList(List<domain.DriverDTO> list)
        {
            var newList = new List<Drivr>();

            foreach(var item in list)
            {
                newList.Add(new Drivr()
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    assignments = item.Assignments
                });
            }
            return newList;
        }

        public static List<DriverAssignment> GetAssignmentList(List<domain.AssignmentDTO> list)
        {
            var newList = new List<DriverAssignment>();
            
            foreach(var item in list)
            {
                string status = "No";
                if( item.complete == 1)
                {
                    status = "Yes";
                }
                newList.Add(new DriverAssignment()
                {
                    aId = item.aId,
                    driverId = item.driverId,
                    details = item.details,
                    sAddress = item.sAddress,
                    eAddress = item.eAddress,
                    complete = status,
                    drivr = item.drivr
                });
            }
            return newList;
        }
    }
}