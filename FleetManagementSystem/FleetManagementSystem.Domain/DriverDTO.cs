﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagementSystem.Domain
{
    public class DriverDTO
    {
        public int Id { get; set; }

        [DataType(DataType.Text)]
        [Display(Name ="First Name")]
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string handle { get; set; }
        public string pass { get; set; }

        public virtual IEnumerable<DataAccess.Assignment> Assignments { get; set; }
    }
}
