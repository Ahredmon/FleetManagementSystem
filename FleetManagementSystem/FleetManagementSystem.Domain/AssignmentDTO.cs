﻿using FleetManagementSystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagementSystem.Domain
{
    public class AssignmentDTO
    {
        public int aId { get; set; }
        public int driverId { get; set; }
        public string details { get; set; }
        public string sAddress { get; set; }
        public string eAddress { get; set; }
        public int complete { get; set; }

        public Driver drivr { get; set; }
    }
}
