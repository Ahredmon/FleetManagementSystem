﻿using FleetManagementSystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagementSystem.Domain
{
    public class DomainHelper
    {
        private DataHelper helper = new DataHelper();

        public bool AddAssignment(AssignmentDTO assign)
        {
            var a = AssignmentMapper.DomainToAccess(assign);
            return helper.AddAssignment(a);
        }

        public bool updateC(AssignmentDTO c)
        {
            var a = AssignmentMapper.DomainToAccess(c);
            return helper.updateC(a);
        }

        public bool login(DriverDTO d)
        {
            var uList = helper.GetDriverList();
            var users = DriverList(uList);

            foreach(var item in users)
            {
                if( item.handle == d.handle && item.pass == d.pass)
                {
                    d.Id = item.Id;
                    return true;
                }
            }
            return false;
            
        }

        public List<DriverDTO> GetDriverList()
        {
            var dList = helper.GetDriverList();
            var drivrs = DriverList(dList);
            return drivrs;
        }

        private List<DriverDTO> DriverList(List<Drivr> list)
        {
            var newList = new List<DriverDTO>();
            foreach(var item in list)
            {
                newList.Add(new DriverDTO()
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    handle = item.handle,
                    pass = item.pass
                });               
            }
            return newList;
        }

        public List<AssignmentDTO> GetAssignList()
        {
            var aList = helper.GetAssignList();
            var assignments = AssignList(aList);
            return assignments;
        }

        private List<AssignmentDTO> AssignList(List<DriverAssignment> list)
        {
            var newList = new List<AssignmentDTO>();
            foreach(var item in list)
            {
                newList.Add(new AssignmentDTO()
                {
                    aId = item.aId,
                    driverId = item.driverId,
                    details = item.details,
                    sAddress = item.sAddress,
                    eAddress = item.eAddress,
                    complete = item.complete,
                    drivr = item.drivr
                });   
            }
            return newList;
        }
    }
}
