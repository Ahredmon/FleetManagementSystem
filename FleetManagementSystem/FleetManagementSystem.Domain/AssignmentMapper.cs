﻿using FleetManagementSystem.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagementSystem.Domain
{
    public class AssignmentMapper
    {
        //change back to DAOvvv
        public static DriverAssignment DomainToAccess(AssignmentDTO a)
        {
            var c = new DriverAssignment();
            c.aId = a.aId;
            c.driverId = a.driverId;
            c.sAddress = a.sAddress;
            c.eAddress = a.eAddress;
            c.details = a.details;
            c.complete = a.complete;
            c.drivr = a.drivr;

            return c;
        }
    }
}
