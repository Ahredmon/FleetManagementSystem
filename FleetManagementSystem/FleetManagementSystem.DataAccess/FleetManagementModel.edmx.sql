
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/01/2016 15:18:44
-- Generated from EDMX file: C:\Revature\FleetManagementSystem\FleetManagementSystem\FleetManagementSystem.DataAccess\FleetManagementModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [FleetManagementDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[fleet].[FK_Dispatch]', 'F') IS NOT NULL
    ALTER TABLE [fleet].[Driver] DROP CONSTRAINT [FK_Dispatch];
GO
IF OBJECT_ID(N'[fleet].[FK_DriverA]', 'F') IS NOT NULL
    ALTER TABLE [fleet].[Assignment] DROP CONSTRAINT [FK_DriverA];
GO
IF OBJECT_ID(N'[fleet].[FK_DriverD]', 'F') IS NOT NULL
    ALTER TABLE [fleet].[Vehicle] DROP CONSTRAINT [FK_DriverD];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[fleet].[Assignment]', 'U') IS NOT NULL
    DROP TABLE [fleet].[Assignment];
GO
IF OBJECT_ID(N'[fleet].[Dispatcher]', 'U') IS NOT NULL
    DROP TABLE [fleet].[Dispatcher];
GO
IF OBJECT_ID(N'[fleet].[Driver]', 'U') IS NOT NULL
    DROP TABLE [fleet].[Driver];
GO
IF OBJECT_ID(N'[fleet].[Vehicle]', 'U') IS NOT NULL
    DROP TABLE [fleet].[Vehicle];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Dispatchers'
CREATE TABLE [dbo].[Dispatchers] (
    [DispatchId] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(25)  NULL,
    [LastName] nvarchar(25)  NULL,
    [handle] nvarchar(50)  NULL,
    [pass] nvarchar(25)  NULL
);
GO

-- Creating table 'Drivers'
CREATE TABLE [dbo].[Drivers] (
    [DriverId] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(25)  NULL,
    [LastName] nvarchar(25)  NULL,
    [handle] nvarchar(50)  NULL,
    [pass] nvarchar(25)  NULL,
    [DispatcherFK] int  NOT NULL
);
GO

-- Creating table 'Vehicles'
CREATE TABLE [dbo].[Vehicles] (
    [VehicleId] int IDENTITY(1,1) NOT NULL,
    [DriverFK] int  NOT NULL,
    [DutyType] nvarchar(25)  NULL,
    [Stat] nvarchar(100)  NULL,
    [Active] bit  NULL
);
GO

-- Creating table 'Assignments'
CREATE TABLE [dbo].[Assignments] (
    [AssignmentId] int IDENTITY(1,1) NOT NULL,
    [DriverFK] int  NOT NULL,
    [Details] nvarchar(100)  NULL,
    [Addr1] nvarchar(200)  NULL,
    [Addr2] nvarchar(200)  NULL,
    [Complete] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [DispatchId] in table 'Dispatchers'
ALTER TABLE [dbo].[Dispatchers]
ADD CONSTRAINT [PK_Dispatchers]
    PRIMARY KEY CLUSTERED ([DispatchId] ASC);
GO

-- Creating primary key on [DriverId] in table 'Drivers'
ALTER TABLE [dbo].[Drivers]
ADD CONSTRAINT [PK_Drivers]
    PRIMARY KEY CLUSTERED ([DriverId] ASC);
GO

-- Creating primary key on [VehicleId] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [PK_Vehicles]
    PRIMARY KEY CLUSTERED ([VehicleId] ASC);
GO

-- Creating primary key on [AssignmentId] in table 'Assignments'
ALTER TABLE [dbo].[Assignments]
ADD CONSTRAINT [PK_Assignments]
    PRIMARY KEY CLUSTERED ([AssignmentId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [DispatcherFK] in table 'Drivers'
ALTER TABLE [dbo].[Drivers]
ADD CONSTRAINT [FK_Dispatch]
    FOREIGN KEY ([DispatcherFK])
    REFERENCES [dbo].[Dispatchers]
        ([DispatchId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Dispatch'
CREATE INDEX [IX_FK_Dispatch]
ON [dbo].[Drivers]
    ([DispatcherFK]);
GO

-- Creating foreign key on [DriverFK] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [FK_DriverD]
    FOREIGN KEY ([DriverFK])
    REFERENCES [dbo].[Drivers]
        ([DriverId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverD'
CREATE INDEX [IX_FK_DriverD]
ON [dbo].[Vehicles]
    ([DriverFK]);
GO

-- Creating foreign key on [DriverFK] in table 'Assignments'
ALTER TABLE [dbo].[Assignments]
ADD CONSTRAINT [FK_DriverA]
    FOREIGN KEY ([DriverFK])
    REFERENCES [dbo].[Drivers]
        ([DriverId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverA'
CREATE INDEX [IX_FK_DriverA]
ON [dbo].[Assignments]
    ([DriverFK]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------