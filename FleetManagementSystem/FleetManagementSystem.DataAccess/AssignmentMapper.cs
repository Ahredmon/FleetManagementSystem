﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetManagementSystem.DataAccess
{
    public class AssignmentMapper
    {
        public static Assignment AccessToDB(DriverAssignment a)
        {
            var p = new Assignment();
            p.AssignmentId = a.aId;
            p.DriverFK = a.driverId;
            p.Addr1 = a.sAddress;
            p.Addr2 = a.eAddress;
            p.Details = a.details;
            p.Complete = a.complete;
            p.Driver = a.drivr;

            return p;
        }
    }
}
