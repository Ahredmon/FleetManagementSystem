﻿using FleetManagementSystem.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FleetManagementSystem.Tests
{
    public class DrivrTests
    {
        private DomainHelper dh = new DomainHelper();

        [Fact]
        public void loginTest()
        {
            DriverDTO d = new DriverDTO()
            {
                handle = "mk",
                pass = "123"
            };

            var expected = true;
            var actual = dh.login(d);

            Assert.True(expected == actual);
        }

        [Fact]
        public void addAssignmentTest()
        {
            AssignmentDTO a = new AssignmentDTO()
            {
                driverId = 2,
                eAddress = "texas",
                sAddress = "california",
                details = "go",
                complete = 0
            };
            var expected = true;
            var actual = dh.AddAssignment(a);

            Assert.True(expected == actual);
        }

        [Fact]
        public void updateTest()
        {
            AssignmentDTO a = new AssignmentDTO()
            {
                driverId = 1,
                aId = 1,
                complete = 1
            };
            var actual = dh.updateC(a);
        }
    }
}
