use master;

--drop database FleetManagementSystemDB;
--go

create database FleetManagementSystemDB;
go

use FleetManagementSystemDB;
go

create schema fleet;
go
 
create table fleet.Driver
(
  DriverId int not null identity(1,1)
  ,FirstName nvarchar(25)
  ,LastName nvarchar(25)
  ,handle nvarchar(50)
  ,pass nvarchar(25)
  ,DispatcherFK int not null
)

create table fleet.Dispatcher
(
  DispatchId int not null identity(1,1)
  ,FirstName nvarchar(25)
  ,LastName nvarchar(25)
  ,handle nvarchar(50)
  ,pass nvarchar(25)
)

create table fleet.Vehicle
(
  VehicleId int not null identity(1,1)
  ,DriverFK int not null
  ,DutyType nvarchar(25)
  ,Stat nvarchar(100)
  ,Active bit null
)

create table fleet.Assignment
(
  AssignmentId int not null identity(1,1)
  ,DriverFK int not null
  ,Details nvarchar(100)
  ,Addr1 nvarchar(200)
  ,Addr2 nvarchar(200)
  ,Complete int not null
)

alter table fleet.Driver
  add constraint PK_DriverId primary key clustered(DriverId);
go

alter table fleet.Dispatcher
  add constraint PK_DispatchId primary key clustered(DispatchId);
go

alter table fleet.Vehicle
  add constraint PK_VehicleId primary key clustered(VehicleId);
go

alter table fleet.Assignment
  add constraint PK_AssignId primary key clustered(AssignmentId);
go

--3 FKs
alter table fleet.Driver
  add constraint FK_Dispatch foreign key (DispatcherFK) references fleet.Dispatcher(DispatchId);
go

alter table fleet.Vehicle
  add constraint FK_DriverD foreign key (DriverFK) references fleet.Driver(DriverId);
go

alter table fleet.Assignment
  add constraint FK_DriverA foreign key (DriverFK) references fleet.Driver(DriverId);
go


--inserting into the database
insert into fleet.Dispatcher(FirstName,LastName,handle,pass)
 values('Jesus', 'Chavez', 'jschavez', '123');
go

insert into fleet.Driver(FirstName,LastName,handle,pass,DispatcherFK)
 values('Jesus', 'C', 'asd', '123', 1),('Maggie','Kohn','mk','123',1),
 ('Isaias', 'Cervantes', 'iicervantes', '123',1);
go

insert into fleet.Driver(FirstName,LastName,handle,pass,DispatcherFK)
 values('Cory', 'Dave', 'asd', '123', 1),('David','Soche','asdf','123',1),
 ('I', 'Dervantes', 'iDervantes', '123',1);
go

insert into fleet.Vehicle(DriverFK,DutyType,Stat)
  values(1,'taxi','in service, on assignment');
go

insert into fleet.Assignment(DriverFK,Details,Addr1,Addr2,Complete)
  values(1,'pick up and drop off', '4111-Trey-Dr,Edinburg,TX', '1718-W-University,Edinburg,TX', 0),
  (2,'pick up and drop off', '1718-W-University,Edinburg,TX', '1201-W-University,Edinburg,TX', 0),
  (3,'pick up and drop off', '4111-Trey-Dr,Edinburg,TX', '1201-W-University,Edinburg,TX', 0);
go

insert into fleet.Assignment(DriverFK,Details,Addr1,Addr2,Complete)
  values(4,'pick up and drop off', '4111-Trey-Dr,Edinburg,TX', '1718-W-University,Edinburg,TX', 0),
  (5,'pick up and drop off', '1718-W-University,Edinburg,TX', '1201-W-University,Edinburg,TX', 0),
  (6,'pick up and drop off', '4111-Trey-Dr,Edinburg,TX', '1201-W-University,Edinburg,TX', 0);
go

insert into fleet.Assignment(DriverFK,Details,Addr1,Addr2,Complete)
  values(2,'pick up and drop off', 'Reston,VA', 'Las-Vegas,NV', 0);
go


select *
from fleet.Driver
inner join fleet.assignment
on fleet.driver.driverid = fleet.assignment.driverfk;
go


select *
from fleet.Assignment;
go

select *
from fleet.Driver;
go

select *
from fleet.Dispatcher;
go

select *
from fleet.Vehicle;
go

--stored procedure, add assignment to table for driverFK
-- actor=dispatcher
create procedure fleet.sp_addassignment @DriverId int, @details nvarchar(100), @startlocation nvarchar(200), @endlocation nvarchar(200), @completed bit
as
begin
  begin transaction
    begin try
      insert into fleet.Assignment(DriverFK,Details,Addr1,Addr2,Complete)
      values(@DriverId,@details,@startlocation,@endlocation,@completed)
      commit transaction
    end try
    begin catch
      rollback transaction
    end catch
end;
go    

--sans try catch
create procedure fleet.sp_addassignmentT @DriverId int, @details nvarchar(100), @startlocation nvarchar(200), @endlocation nvarchar(200), @completed bit
as
begin
      insert into fleet.Assignment(DriverFK,Details,Addr1,Addr2,Complete)
      values(@DriverId,@details,@startlocation,@endlocation,@completed)
end;
go

--stored procedure, set completion status to assignment table 
-- actor=driver
create procedure fleet.sp_updateCompleteStatus @Assign int, @completed bit
as
begin
  begin transaction
    begin try
      update fleet.Assignment
      set Complete = @completed
      where AssignmentId = @Assign
      commit transaction
    end try
    begin catch
      rollback transaction
    end catch
end;
go

--sans try catch
create procedure fleet.sp_updateCompleteStatusT @Assign int, @completed bit
as
begin
      update fleet.Assignment
      set Complete = @completed
      where AssignmentId = @Assign
end;
go
